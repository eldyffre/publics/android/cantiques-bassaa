package com.android.eldyffre.cantiques.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cantique implements Serializable, Parcelable
{

    @SerializedName("numero")
    @Expose
    private int numero;
    @SerializedName("titre")
    @Expose
    private String titre;
    @SerializedName("auteur")
    @Expose
    private String auteur;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("favori")
    @Expose
    private boolean favori;
    @SerializedName("vers")
    @Expose
    private ArrayList<Vers> vers = new ArrayList<Vers>();
    @SerializedName("references")
    @Expose
    private ArrayList<Reference> references = new ArrayList<Reference>();
    public final static Creator<Cantique> CREATOR = new Creator<Cantique>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Cantique createFromParcel(Parcel in) {
            return new Cantique(in);
        }

        public Cantique[] newArray(int size) {
            return (new Cantique[size]);
        }

    }
            ;
//    private final static long serialVersionUID = 4820997503059764746L;

    private Cantique(Parcel in) {
        this.numero = ((int) in.readValue((int.class.getClassLoader())));
        this.titre = ((String) in.readValue((String.class.getClassLoader())));
        this.auteur = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.favori = ((boolean) in.readValue((boolean.class.getClassLoader())));
        in.readList(this.vers, (com.android.eldyffre.cantiques.beans.Vers.class.getClassLoader()));
        in.readList(this.references, (com.android.eldyffre.cantiques.beans.Reference.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Cantique() {
    }

    /**
     *
     * @param references
     * @param vers
     * @param description
     * @param favori
     * @param auteur
     * @param titre
     * @param numero
     */
    public Cantique(int numero, String titre, String auteur, String description, boolean favori, ArrayList<Vers> vers, ArrayList<Reference> references) {
        super();
        this.numero = numero;
        this.titre = titre;
        this.auteur = auteur;
        this.description = description;
        this.favori = favori;
        this.vers = vers;
        this.references = references;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTitre() {
        return titre;
    }

    public String getStringNumero() {

        String numero = String.valueOf(this.numero);

        while (numero.length() < 3){
            numero = "0".concat(numero);
        }

        return numero;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFavori() {
        return favori;
    }

    public void setFavori(boolean favori) {
        this.favori = favori;
    }

    public ArrayList<Vers> getvers() {
        return vers;
    }

    public void setvers(ArrayList<Vers> vers) {
        this.vers = vers;
    }

    public ArrayList<Reference> getReferences() {
        return references;
    }

    public void setReferences(ArrayList<Reference> references) {
        this.references = references;
    }

    @Override
    public String toString() {
        return this.titre;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(numero);
        dest.writeValue(titre);
        dest.writeValue(auteur);
        dest.writeValue(description);
        dest.writeValue(favori);
        dest.writeList(vers);
        dest.writeList(references);
    }

    public int describeContents() {
        return 0;
    }

}
