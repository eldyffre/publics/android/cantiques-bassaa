package com.android.eldyffre.cantiques.views;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.eldyffre.cantiques.R;
import com.android.eldyffre.cantiques.beans.Vers;

import java.util.ArrayList;

public class VersAdapter extends RecyclerView.Adapter<VersAdapter.ViewHolder>{

    private ArrayList<Vers> vers = null;

    private ArrayList<Vers> arrayList;

    public VersAdapter(ArrayList<Vers> vers) {
        this.arrayList = vers;

        this.vers = new ArrayList<>();

        this.vers.addAll(arrayList);
    }

    @Override
    public VersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ligne_de_vers, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VersAdapter.ViewHolder holder, int position) {
        final Vers vers = this.vers.get(position);

        holder.bind(vers);

    }

    @Override
    public int getItemCount() {
        return this.vers == null ? 0 : this.vers.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_numero_vers;
        TextView tv_contenu_vers;

        public ViewHolder(View itemView) {
            super(itemView);

            this.tv_contenu_vers = itemView.findViewById(R.id.tv_contenu_vers);
            this.tv_numero_vers = itemView.findViewById(R.id.tv_numero_vers);

        }

        public  void bind(final Vers vers){
            this.tv_numero_vers.setText(String.valueOf(vers.getNumero()));
            if (vers.isRefrain()){
                this.tv_contenu_vers.setText(Html.fromHtml("<strong>Iniyinma<strong><br/>" + vers.getContenu()));
            }else {
                this.tv_contenu_vers.setText(vers.getContenu());
            }

        }
    }


}
