package com.android.eldyffre.cantiques;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

public class SummaryActivity extends AppCompatActivity implements View.OnClickListener {


    //UI Components
    CardView cv_preface;
    CardView cv_participants;
    CardView cv_cantiques;
    CardView cv_com_relecture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        getSupportActionBar().setTitle("Sommaire");

        initVUIViews();
    }


    private void initVUIViews(){
        this.cv_cantiques = findViewById(R.id.cv_cantiques);
        this.cv_com_relecture = findViewById(R.id.cv_com_relecture);
        this.cv_participants = findViewById(R.id.cv_participants);
        this.cv_preface = findViewById(R.id.cv_preface);


        this.cv_cantiques.setOnClickListener(this);
        this.cv_com_relecture.setOnClickListener(this);
        this.cv_participants.setOnClickListener(this);
        this.cv_preface.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == null)
            return;

        switch (view.getId()){
            case R.id.cv_cantiques:
                Intent intent = new Intent(this, CantiquesListActivity.class);
                startActivity(intent);

                break;
            case R.id.cv_com_relecture:

                new MaterialDialog.Builder(this)
                                .title(R.string.app_name)
                                .items(R.array.participants)
                                .iconRes(R.drawable.lecture48)
                                .positiveText(R.string.ok)
                                .show();
                break;

            case R.id.cv_participants:

                new MaterialDialog.Builder(this)
                        .title(R.string.app_name)
                        .items(R.array.participants)
                        .iconRes(R.drawable.coworking48)
                        .positiveText(R.string.ok)
                        .show();
                break;

            case R.id.cv_preface:

                new MaterialDialog.Builder(this)
                        .title(R.string.app_name)
                        .content(R.string.app_name)
                        .iconRes(R.drawable.preface)
                        .positiveText(R.string.ok)
                        .show();

                break;
        }
    }
}
