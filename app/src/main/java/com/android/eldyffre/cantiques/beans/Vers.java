package com.android.eldyffre.cantiques.beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vers implements Serializable, Parcelable {

    @SerializedName("numero")
    @Expose
    private int numero;
    @SerializedName("contenu")
    @Expose
    private String contenu;
    @SerializedName("refrain")
    @Expose
    private boolean refrain;
    public final static Creator<Vers> CREATOR = new Creator<Vers>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Vers createFromParcel(Parcel in) {
            return new Vers(in);
        }

        public Vers[] newArray(int size) {
            return (new Vers[size]);
        }

    };
//    private final static long serialVersionUID = -8631007545169098289L;

    protected Vers(Parcel in) {
        this.numero = ((int) in.readValue((int.class.getClassLoader())));
        this.contenu = ((String) in.readValue((String.class.getClassLoader())));
        this.refrain = ((boolean) in.readValue((boolean.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Vers() {
    }

    /**
     * @param contenu
     * @param refrain
     * @param numero
     */
    public Vers(int numero, String contenu, boolean refrain) {
        super();
        this.numero = numero;
        this.contenu = contenu;
        this.refrain = refrain;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public boolean isRefrain() {
        return refrain;
    }

    public void setRefrain(boolean refrain) {
        this.refrain = refrain;
    }

    @Override
    public String toString() {
        return this.contenu;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(numero);
        dest.writeValue(contenu);
        dest.writeValue(refrain);
    }

    public int describeContents() {
        return 0;
    }
}
