package com.android.eldyffre.cantiques.utils;

import android.content.Context;

import com.android.eldyffre.cantiques.R;
import com.android.eldyffre.cantiques.beans.Cantique;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONUtils {
    public static String DATA_FILE = "datas.json";

    public static boolean Write(Context context, ArrayList<Cantique> cantiques){

        String content = new Gson().toJson(cantiques);

        FileOutputStream outputStream;

        try {
            outputStream = context.openFileOutput(DATA_FILE, Context.MODE_PRIVATE);
            outputStream.write(content.getBytes());
            outputStream.close();

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static ArrayList<Cantique> Read(Context context){

        ArrayList<Cantique> cantiques = new ArrayList<>();

        File file = new File(context.getFilesDir(), DATA_FILE);

        if (!file.exists()){
            InputStream fis = context.getResources().openRawResource(com.android.eldyffre.cantiques.R.raw.cantiques);
            JsonReader reader = new JsonReader(new InputStreamReader(fis));

            cantiques = new Gson().fromJson(reader, new TypeToken<ArrayList<Cantique>>(){}.getType());

        }else {

            try {
                FileInputStream inputStream = context.openFileInput(DATA_FILE);
                JsonReader reader = new JsonReader(new InputStreamReader(inputStream));

                cantiques = new Gson().fromJson(reader,  new TypeToken<ArrayList<Cantique>>(){}.getType());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return cantiques;
    }
}
