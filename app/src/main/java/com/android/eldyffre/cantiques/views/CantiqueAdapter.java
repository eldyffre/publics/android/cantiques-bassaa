package com.android.eldyffre.cantiques.views;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.eldyffre.cantiques.R;
import com.android.eldyffre.cantiques.beans.Cantique;

import java.util.ArrayList;
import java.util.Locale;

public class CantiqueAdapter extends RecyclerView.Adapter<CantiqueAdapter.ViewHolder>{

    private ArrayList<Cantique> cantiques = null;
    private OnCantiqueClickListener listener;

    private ArrayList<Cantique> arrayList;

    public CantiqueAdapter(ArrayList<Cantique> cantiques, OnCantiqueClickListener listener) {
        this.arrayList = cantiques;

        this.cantiques = new ArrayList<>();

        this.cantiques.addAll(arrayList);
        this.listener = listener;
    }

    @Override
    public CantiqueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ligne_de_cantique, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CantiqueAdapter.ViewHolder holder, int position) {
        final Cantique cantique = this.cantiques.get(position);

        holder.bind(cantique);

        if (!(this.listener == null)){
            holder.rel_line_cantique.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(cantique);
                }
            });

            holder.iv_favorite_cantique.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onFavoriClick(cantique);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return this.cantiques == null ? 0 : this.cantiques.size();
    }


    /**
     * @param query search pattern, used to perform your query
     */
    public void filter(String query){
        query = query.toLowerCase(Locale.getDefault());

        this.cantiques.clear();

        if (query.length() == 0){
            this.cantiques.addAll(arrayList);
        }else {
            for (Cantique cantique : this.arrayList) {
                if (cantique.getStringNumero().contains(query)){
                    this.cantiques.add(cantique);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void getFavorites(){
        this.cantiques.clear();

        for (Cantique cantique : this.arrayList) {
            if (cantique.isFavori()){
                this.cantiques.add(cantique);
            }
        }
        notifyDataSetChanged();
    }

    public ArrayList<Cantique> getCantiques() {
        return this.cantiques;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tv_numero_cantique;
        TextView tv_auteur_cantique;
        TextView tv_titre_cantique;
        ImageView iv_favorite_cantique;

        RelativeLayout rel_line_cantique;

        public ViewHolder(View itemView) {
            super(itemView);

            this.iv_favorite_cantique = itemView.findViewById(R.id.iv_favorite_cantique);
            this.tv_auteur_cantique = itemView.findViewById(R.id.tv_auteur_cantique);
            this.tv_numero_cantique = itemView.findViewById(R.id.tv_numero_cantique);
            this.tv_titre_cantique = itemView.findViewById(R.id.tv_titre_cantique);
            this.rel_line_cantique = itemView.findViewById(R.id.rel_line_cantique);
        }

        public  void bind(final Cantique cantique){
            this.tv_numero_cantique.setText(cantique.getStringNumero());
            this.tv_titre_cantique.setText(cantique.getTitre());
            this.tv_auteur_cantique.setText(cantique.getAuteur());

            final boolean isFavorite = cantique.isFavori();

            if (isFavorite)
                this.iv_favorite_cantique.setImageResource(R.drawable.ic_star_filled);
            else
                this.iv_favorite_cantique.setImageResource(R.drawable.ic_star);
        }
    }

    public interface OnCantiqueClickListener{
        void onClick(Cantique cantique);
        void onFavoriClick(Cantique cantique);
    }

}
