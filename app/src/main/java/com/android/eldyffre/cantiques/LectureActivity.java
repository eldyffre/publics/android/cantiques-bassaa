package com.android.eldyffre.cantiques;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.eldyffre.cantiques.beans.Cantique;
import com.android.eldyffre.cantiques.beans.Reference;
import com.android.eldyffre.cantiques.beans.Vers;
import com.android.eldyffre.cantiques.views.PlaceholderFragment;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Locale;

public class LectureActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    ArrayList<Cantique> cantiques;
    Cantique currentCantique;

    int currentIndex;


    FloatingActionButton fab_previous;
    FloatingActionButton fab_next;

    Toolbar toolbar;

    MaterialSearchView msv_search_view_lecture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecture);

        toolbar = (Toolbar) findViewById(R.id.toolbar_lecture);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initUIViews();

        Intent intent = getIntent();

        if (intent == null)
            super.onBackPressed();

        cantiques = intent.getParcelableArrayListExtra("CurrentCollection");

        if (cantiques == null)
            super.onBackPressed();

        int index = intent.getIntExtra("CurrentIndex",0);

        System.out.println("CurrentIndex - LectureActivity : " + index);

        currentCantique = this.cantiques.get(index);
        currentIndex = index;


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.vp_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        this.navigateTo(index);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.toolbar.inflateMenu(R.menu.menu_lecture);


        this.toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return onOptionsItemSelected(item);
                    }
                });

        MenuItem item = menu.findItem(R.id.sv_action_search_lecture);
        msv_search_view_lecture.setMenuItem(item);

        msv_search_view_lecture.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Cantique cantique = search(query);

                if (cantique == null){
                    Snackbar.make(msv_search_view_lecture, "Aucun cantique ne porte le numéro renseigné! Avez vous fait une erreur ?", 7000);
                    return false;
                }

                navigateTo(cantiques.indexOf(cantique));

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        msv_search_view_lecture.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
            }
        });


        return super.onCreateOptionsMenu(menu);
    }

    Cantique search(String query){
        query = query.toLowerCase(Locale.getDefault());

        if (!(query.length() == 0)){
            for (Cantique cantique : this.cantiques) {
                if (cantique.getStringNumero() == query){
                    return cantique;
                }
            }
        }

        return null;
    }

    void initUIViews(){
        this.msv_search_view_lecture = findViewById(R.id.msv_search_view_lecture);

        this.fab_next = (FloatingActionButton) findViewById(R.id.fab_next);
        this.fab_previous = (FloatingActionButton) findViewById(R.id.fab_previous);

        this.fab_previous.setOnClickListener(this);
        this.fab_next.setOnClickListener(this);
    }

    void navigateTo(int index){
        if (index < 0 || index >= this.cantiques.size())
            return;

        this.currentIndex = index;
        this.currentCantique = this.cantiques.get(index);

        mViewPager.setCurrentItem(index, true);
        toolbar.setTitle(currentCantique.getTitre());

        if (currentIndex == (this.cantiques.size() - 1)){
            this.fab_next.setVisibility(View.INVISIBLE);
            this.fab_next.setEnabled(false);
        }else {
            this.fab_next.setVisibility(View.VISIBLE);
            this.fab_next.setEnabled(true);
        }

        if (currentIndex == 0){
            this.fab_previous.setVisibility(View.INVISIBLE);
            this.fab_previous.setEnabled(false);
        }else {
            this.fab_previous.setVisibility(View.VISIBLE);
            this.fab_previous.setEnabled(true);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == null)
            return;

        switch (view.getId()){
            case R.id.fab_next :
                    navigateTo(currentIndex + 1);
                break;

            case R.id.fab_previous:
                    navigateTo(currentIndex - 1);
                break;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            PlaceholderFragment placeholderFragment = new PlaceholderFragment().newInstance(position + 1);

            Bundle bundle = new Bundle();

            bundle.putParcelable("CANTIQUE", currentCantique);

            placeholderFragment.setArguments(bundle);

            return placeholderFragment;
        }



        @Override
        public int getCount() {

            return cantiques.size();
        }
    }
}
