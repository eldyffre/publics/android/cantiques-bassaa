package com.android.eldyffre.cantiques.views;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.eldyffre.cantiques.R;
import com.android.eldyffre.cantiques.beans.Cantique;
import com.android.eldyffre.cantiques.beans.Reference;
import com.android.eldyffre.cantiques.beans.Vers;

public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public PlaceholderFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_lecture, container, false);

        Cantique cantique = getArguments().getParcelable("CANTIQUE");

        if (cantique == null)
            return rootView;

        TextView tv_references = rootView.findViewById(R.id.tv_references);
//        TextView tv_contenu_cantique = rootView.findViewById(R.id.tv_contenu_cantique);
        TextView tv_auteur_cantique_lecture = rootView.findViewById(R.id.tv_auteur_cantique_lecture);

        RecyclerView rv_vers_lecture = rootView.findViewById(R.id.rv_vers_lecture);

        VersAdapter adapter = new VersAdapter(cantique.getvers());

        rv_vers_lecture.setAdapter(adapter);

        rv_vers_lecture.setLayoutManager(new LinearLayoutManager(rootView.getContext()));

        adapter.notifyDataSetChanged();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            //tv_auteur_cantique_lecture.setText(Html.fromHtml("<span style=\" \"", Html.FROM_HTML_SEPARATOR_LINE_BREAK_DIV));

            tv_auteur_cantique_lecture.setText(cantique.getAuteur());
            tv_references.setText(Html.fromHtml(formatCantiqueReferences(cantique)));
//            tv_contenu_cantique.setText(Html.fromHtml(formatCantiqueVers(cantique)));

        }else {

            tv_auteur_cantique_lecture.setText(cantique.getAuteur());
            tv_references.setText(formatCantiqueReferences(cantique));
//            tv_contenu_cantique.setText(formatCantiqueVers(cantique));
        }

        return rootView;
    }

    String formatCantiqueReferences(Cantique cantique){

        String result = "";

        for (Reference ref : cantique.getReferences()) {
            result += ref.getNom() + " " + ref.getNumero() + " ( " + ref.getLangue() + ")";
            if (!(cantique.getReferences().indexOf(ref) == cantique.getReferences().size()))
                result += "\n";
        }

        if (result.length() == 0){
            result = "Pas de références !";
        }

        System.out.println(cantique.getTitre() + " (références) -> " +result);

        return result;
    }

    String formatCantiqueVers(Cantique cantique){

        String result = "";

        for (Vers vers : cantique.getvers()) {

            if (vers.isRefrain()){
                result += Html.fromHtml("<strong>Refrain : <br/> " + vers.getContenu() + "</strong>");
            }else {
                result += String.valueOf(vers.getNumero()) + ".  " + vers.getContenu();
            }

            if (!(cantique.getvers().indexOf(vers) == cantique.getvers().size()))
                result += "\n";
        }

        System.out.println(cantique.getTitre() + " (vers) -> " + result);

        return result;
    }

}

