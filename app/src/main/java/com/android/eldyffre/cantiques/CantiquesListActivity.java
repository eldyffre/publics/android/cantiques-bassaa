package com.android.eldyffre.cantiques;

import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.eldyffre.cantiques.beans.Cantique;
import com.android.eldyffre.cantiques.utils.JSONUtils;
import com.android.eldyffre.cantiques.views.CantiqueAdapter;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;

import static com.android.eldyffre.cantiques.utils.JSONUtils.Write;

public class CantiquesListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Cantique>>,CantiqueAdapter.OnCantiqueClickListener, View.OnClickListener {

    RecyclerView rv_cantiques_list;
    CantiqueAdapter adapter;

    ArrayList<Cantique> cantiques;

    Toolbar toolbar;

    MaterialDialog materialDialog;

    FetchData asyncLoader;

    LoaderManager loaderManager;

    MenuItem mnu_option_cancel_favoris;

    MenuItem mnu_option_show_favoris;

    MaterialSearchView searchView;

    FloatingActionButton fab_clear_favorites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cantiques_list);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("Tous les cantiques");

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.fab_clear_favorites = findViewById(R.id.fab_clear_favorites);
        this.fab_clear_favorites.setOnClickListener(this);
        this.fab_clear_favorites.setVisibility(View.GONE);

        this.rv_cantiques_list = findViewById(R.id.rv_cantiques_list);
        rv_cantiques_list.setLayoutManager(new LinearLayoutManager(this));

        searchView = (MaterialSearchView) findViewById(R.id.msv_search_view);
//        new FetchData(this).loadInBackground();

        materialDialog = new MaterialDialog.Builder(this)
                .content("Veuillez patienter, SVP !")
                .progressIndeterminateStyle(false)
                .progress(true, 0)
                .canceledOnTouchOutside(false)
                .show();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                materialDialog.dismiss();
                cantiques = JSONUtils.Read(CantiquesListActivity.this);
                adapter = new CantiqueAdapter(cantiques, CantiquesListActivity.this);
                adapter.notifyDataSetChanged();
                rv_cantiques_list.setAdapter(adapter);

            }
        }, 5000);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.mnu_option_favoris:
                this.adapter.getFavorites();
                this.mnu_option_cancel_favoris.setVisible(true);
                this.mnu_option_show_favoris.setVisible(false);

                this.toolbar.setTitle("Mes Favoris");
                this.fab_clear_favorites.setVisibility(View.VISIBLE);

                break;
            case R.id.mnu_option_apropos:

                break;
            case R.id.mnu_option_chanter:

                break;
            case R.id.mnu_option_contact_us:
                contactUs();
                break;
            case R.id.mnu_option_help:

                break;
            case R.id.mnu_option_share:
                    shareTextUrl();
                break;
            case R.id.mnu_option_cancelfavoris:
                this.adapter.filter("");
                this.mnu_option_show_favoris.setVisible(true);
                this.mnu_option_cancel_favoris.setVisible(false);

                this.toolbar.setTitle("Tous les cantiques");
                this.fab_clear_favorites.setVisibility(View.GONE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    // Method to share either text or URL.
    private void shareTextUrl() {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Télécharger l'application Cantiques Bassa'a et chantons ensemble les merveilles de notre Seigneur.");
        share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/cantiques-bassaas");

        startActivity(Intent.createChooser(share, "Partager Cantiques Bassa'a via"));
    }


    private void contactUs(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"kamgangf11@outlook.fr"});
        intent.putExtra(Intent.EXTRA_CC , new String[] {});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Nous contacter");

        try {
            startActivity(Intent.createChooser(intent, "Envoyer via ?"));
        } catch (android.content.ActivityNotFoundException ex) {
            //do something else
        }
    }

    // Method to share any image.
//    private void shareImage() {
//        Intent share = new Intent(Intent.ACTION_SEND);
//
//        // If you want to share a png image only, you can do:
//        // setType("image/png"); OR for jpeg: setType("image/jpeg");
//        share.setType("image/*");
//
//        // Make sure you put example png image named myImage.png in your
//        // directory
//        String imagePath = Environment.getExternalStorageDirectory()
//                + "/myImage.png";
//
//        File imageFileToShare = new File(imagePath);
//
//        Uri uri = Uri.fromFile(imageFileToShare);
//        share.putExtra(Intent.EXTRA_STREAM, uri);
//
//        startActivity(Intent.createChooser(share, "Share Image!"));
//    }


        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.toolbar.inflateMenu(R.menu.cantique_list_menu);


        this.toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        return onOptionsItemSelected(item);
                    }
                });

        MenuItem item = menu.findItem(R.id.sv_action_search);
        searchView.setMenuItem(item);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.filter(query);
                return true;
                }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

            }

            @Override
            public void onSearchViewClosed() {
                adapter.filter("");
            }
        });

        this.mnu_option_cancel_favoris = menu.findItem(R.id.mnu_option_cancelfavoris);
        this.mnu_option_show_favoris = menu.findItem(R.id.mnu_option_favoris);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public Loader<ArrayList<Cantique>> onCreateLoader(int i, Bundle bundle) {
        return new FetchData(this);
    }

    //TODO : intercepter l'annulatin du loader
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Cantique>> loader, ArrayList<Cantique> cantiques) {
        materialDialog.dismiss();

        if (cantiques == null){
            new MaterialDialog.Builder(this)
                    .title(R.string.app_name)
                    .icon(getResources().getDrawable(R.drawable.logo))
                    .content("Aucune donnée trouvée sur le téléphone. Que voulez-vous faire ??")
                    .positiveText("Retourner au sommaire")
                    .neutralText("Réésayer")
                    .canceledOnTouchOutside(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            onBackPressed();
                        }
                    })
                    .onNeutral(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            loaderManager.getLoader(1).startLoading();
                        }
                    }).show();
            return;
        }

        this.adapter = new CantiqueAdapter(cantiques, this);
        this.adapter.notifyDataSetChanged();
        this.rv_cantiques_list.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Cantique>> loader) {
    }

    @Override
    public void onClick(Cantique cantique) {
        Intent intent = new Intent(CantiquesListActivity.this, LectureActivity.class);
        intent.putParcelableArrayListExtra("CurrentCollection", this.adapter.getCantiques());
        intent.putExtra("CurrentIndex", this.adapter.getCantiques().indexOf(cantique));

        System.out.println("CurrentIndex - CantiquesListActivity : " + this.adapter.getCantiques().indexOf(cantique));

        startActivity(intent);
    }

    @Override
    public void onFavoriClick(Cantique cantique) {
        cantique.setFavori(!cantique.isFavori());
        try{
            Write(this, this.adapter.getCantiques());
            this.adapter.notifyDataSetChanged();
        }catch (Exception e){
            Snackbar.make(this.rv_cantiques_list, "Impossible d'enregistrer vos modifications. Réessayez plus tard svp !", 7000);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == null)
            return;

        switch (view.getId()){


            case R.id.fab_clear_favorites :
                new MaterialDialog.Builder(this)
                        .title("Confirmation requise")
                        .content("Voulez-vous vider votre liste de favoris ? Cette action est irréversible !")
                        .icon(getResources().getDrawable(R.drawable.ic_star))
                        .canceledOnTouchOutside(false)
                        .positiveText("Oui")
                        .negativeText("Non")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                for (Cantique cantique: adapter.getCantiques()) {
                                    cantique.setFavori(false);
                                }
                                onOptionsItemSelected(mnu_option_cancel_favoris);
                                try{
                                    Write(CantiquesListActivity.this, adapter.getCantiques());
                                    adapter.notifyDataSetChanged();
                                }catch (Exception e){
                                    Snackbar.make(rv_cantiques_list, "Impossible d'enregistrer vos modifications. Réessayez plus tard svp !", 7000);
                                }
                            }
                        })
                        .show();

                break;
        }
    }

    private static class FetchData extends AsyncTaskLoader<ArrayList<Cantique>>{

        public FetchData(Context context) {
            super(context);
        }

        @Override
        public ArrayList<Cantique> loadInBackground() {
            return JSONUtils.Read(getContext());
        }

        @Override
        public void deliverResult(ArrayList<Cantique> data) {
            super.deliverResult(data);
        }
    }
}
