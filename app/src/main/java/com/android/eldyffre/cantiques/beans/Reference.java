package com.android.eldyffre.cantiques.beans;

import java.io.Serializable;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reference implements Serializable, Parcelable
{

    @SerializedName("nom")
    @Expose
    private String nom;
    @SerializedName("langue")
    @Expose
    private String langue;
    @SerializedName("numero")
    @Expose
    private int numero;
    public final static Creator<Reference> CREATOR = new Creator<Reference>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Reference createFromParcel(Parcel in) {
            return new Reference(in);
        }

        public Reference[] newArray(int size) {
            return (new Reference[size]);
        }

    };

//    private final static long serialVersionUID = 2051503152224349992L;

    protected Reference(Parcel in) {
        this.nom = ((String) in.readValue((String.class.getClassLoader())));
        this.langue = ((String) in.readValue((String.class.getClassLoader())));
        this.numero = ((int) in.readValue((int.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Reference() {
    }

    /**
     *
     * @param langue
     * @param nom
     * @param numero
     */
    public Reference(String nom, String langue, int numero) {
        super();
        this.nom = nom;
        this.langue = langue;
        this.numero = numero;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return this.langue + " " + this.numero;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(nom);
        dest.writeValue(langue);
        dest.writeValue(numero);
    }

    public int describeContents() {
        return 0;
    }

}
